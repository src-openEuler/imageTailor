# imageTailor

#### Description
a tool for tailoring image

#### Software Architecture
x86_64/aarch64

#### Installation

yum install imageTailor

#### Instructions

cd /opt/imageTailor
./mkdliso -h

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
